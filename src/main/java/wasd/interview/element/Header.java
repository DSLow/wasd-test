package wasd.interview.element;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;


/**
 * @project wasd_test
 * Created by Andrey M on 30/01/2020.
 */
public class Header {

	public SelenideElement menuElement(String text) {
		return $x("//header[@class='header']//a[contains(., '" +text+ "')]");
	}
}

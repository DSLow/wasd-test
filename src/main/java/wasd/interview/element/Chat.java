package wasd.interview.element;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class Chat {

	public SelenideElement settingsMenu() {
		return $x("//div[@class='settings-menu']");
	}

	public SelenideElement chatSetting(String chatSetting) {
		return $x("//div[text()='" +chatSetting+ "']/..//div[@class='slide-toggle__thumb']");
	}

	public Boolean chatSettingIsEnabled(String chatSetting) {
		return $x("//div[text()='" +chatSetting+ "' and @class='slide-toggle__label toggled']").isDisplayed();
	}

	public void enableSetting(String chatSetting) {
		if (chatSettingIsEnabled(chatSetting) == Boolean.FALSE) {
			chatSetting(chatSetting).click();
		}
	}
}

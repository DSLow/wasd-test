package wasd.interview.layout;

import com.codeborne.selenide.Selenide;
import wasd.interview.element.Header;


/**
 * @project wasd_test
 * Created by Andrey M on 30/01/2020.
 */
public class WithHeader {

	public Header header() {
		return Selenide.page(Header.class);
	}
}

package wasd.interview.layout;

import com.codeborne.selenide.Selenide;
import wasd.interview.element.Chat;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class WithChat {

	public Chat chat() {
		return Selenide.page(Chat.class);
	}
}

package wasd.interview.page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class GameCardPage {

	public SelenideElement gameCardTitle() {
		return $x("//div[@class='game-channel__title']");
	}

	public ElementsCollection videosList() {
		return $$x("//wasd-game-streams[2]//a[@class='stream-card__overlay']");
	}

	public ElementsCollection videoTitlesList() {
		return $$x("//wasd-game-streams[2]//a[@class='stream-card__title']");
	}
}

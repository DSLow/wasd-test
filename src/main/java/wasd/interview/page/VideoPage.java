package wasd.interview.page;

import com.codeborne.selenide.SelenideElement;
import wasd.interview.layout.WithChat;

import static com.codeborne.selenide.Selenide.$x;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class VideoPage extends WithChat {

	public SelenideElement videoTitle() {
		return $x("//div[@class='channel__stream-name']");
	}
}

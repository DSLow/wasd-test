package wasd.interview.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class GamesPage {

	public SelenideElement title() {
		return $x("//div[@class='section-title']");
	}

	public SelenideElement nextGameCardTo(String name) {
		return $x(getRootPath(name) + "/following::div[@class='card-game__body--img'][1]");
	}

	public String nextGameCardNameToText(String name) {
		return $x(getRootPath(name) + "/following::div[@class='card-game__title'][1]").getText();
	}

	private String getRootPath(String name) {
		return "//div[@title='" + name + "']/ancestor::div";
	}
}

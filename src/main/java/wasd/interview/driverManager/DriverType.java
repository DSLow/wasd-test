package wasd.interview.driverManager;

/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public enum DriverType {
	CANARY,
	CHROME,
	FIREFOX,
}

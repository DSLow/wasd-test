package wasd.interview.driverManager;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.chrome.ChromeOptions;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class CanaryDriverManager extends DriverManager {

	private static final String CHROME_PATH = "\\AppData\\Local\\Google\\Chrome SxS\\Application\\chrome.exe";

	@Override
	protected void setConfig() {

		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else if (System.getProperty("os.name").contains("Linux")) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriverLinux");
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriverMac");
		}

		Configuration.browserBinary = System.getProperty("user.home") + CHROME_PATH;

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation");
		options.addArguments("--disable-gpu");

		Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
	}
}

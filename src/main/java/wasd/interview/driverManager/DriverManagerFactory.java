package wasd.interview.driverManager;

/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class DriverManagerFactory {

	public static DriverManager getManager(DriverType type) {

		DriverManager driverManager;

		switch (type) {
			case CANARY:
				driverManager = new CanaryDriverManager();
				break;
			case FIREFOX:
				driverManager = new FirefoxDriverManager();
				break;
			default:
				driverManager = new ChromeDriverManager();
				break;
		}

		return driverManager;
	}
}